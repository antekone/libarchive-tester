#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <unistd.h>
#include "libarchive/archive_blake2.h"

#define hexdump(buf, size) \
    for(int i = 0; i < size; i++) { \
        cout << hex << setw(2) << setfill('0') << (int) buf[i] << setw(0); \
    } \
    cout << "\n";

void process(const char* filename) {
    using std::cout;
    using std::hex;
    using std::setw;
    using std::setfill;

    std::vector<char> buf;
    std::vector<uint8_t> b2buf;
    std::ifstream in(filename);

    if(!in) {
        cout << "Can't open file: " << filename << "\n";
        return;
    }

    buf.resize(0x20000);
    blake2sp_state b2state = {};
    blake2sp_init(&b2state, 32);

#define PASS(size) in.read(buf.data(), size); blake2sp_update(&b2state, buf.data(), size);

    PASS(0x5300);
    PASS(0x7C00);
    PASS(0x3100);
    PASS(0xd900);
    PASS(0x2700);
    //PASS(0x10000);

    hexdump(b2state.buf, 8*64);

}

int main(int argc, char **argv) {
    using namespace std;

    if(argc < 2) {
        cout << "Enter filenames as arguments.\n";
        return 1;
    }

    for(int i = 1; i < argc; i++) {
        process(argv[i]);
    }

    return 0;
}
