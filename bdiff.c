#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define min(a, b)  (((a) > (b)) ? (b) : (a))

void dump_diff(FILE* f1, FILE* f2, int off) {
    int i, off_b = off - off % 16;
    char buf[16];

    memset(buf, 0, 16);

    fseek(f1, off_b, SEEK_SET);
    fread(buf, 16, 1, f1);
    printf("F1: 0x%08x ", off_b);
    for(i = 0; i < 16; i++) {
        printf("%02x ", (uint8_t) buf[i]);
    }
    printf("\n");

    fseek(f2, off_b, SEEK_SET);
    fread(buf, 16, 1, f2);
    printf("F2: 0x%08x ", off_b);
    for(i = 0; i < 16; i++) {
        printf("%02x ", (uint8_t) buf[i]);
    }
    printf("\n");
}

int main(int argc, char** argv) {
    if(argc != 3) {
        printf("Two files in arguments please\n");
        return 1;
    }

    char* fn1 = argv[1];
    char* fn2 = argv[2];
    char buf[4096];
    char buf2[4096];

    FILE* f1 = fopen(fn1, "rb");
    FILE* f2 = fopen(fn2, "rb");

    if(!f1) {
        printf("Can't open %s\n", fn1);
        return 1;
    }

    if(!f2) {
        printf("Can't open %s\n", fn2);
        return 1;
    }

    int c = 0;
    int q = 0;
    while(!q) {
        int read1;
        int read2;
        int cmpn, i;

        read1 = fread(buf, 1, sizeof(buf), f1);
        read2 = fread(buf2, 1, sizeof(buf2), f2);

        if(read1 != read2) {
            if(read1 < read2) {
                printf("Input of %s is truncated\n", fn1);
            } else {
                printf("Input of %s is truncated\n", fn2);
            }

            q = 1;
        }

        cmpn = min(read1, read2);
        if(cmpn == 0) break;

        for(i = 0; i < cmpn; i++) {
            if(buf[i] != buf2[i]) {
                printf("Difference in 0x%08x\n", c + i);
                dump_diff(f1, f2, c + i);
                return 1;
            }
        }

        c += cmpn;
    }

    return 0;
}
