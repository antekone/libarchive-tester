#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <unistd.h>
#include "libarchive/archive_blake2.h"

void process(const char* filename) {
    using std::cout;
    using std::hex;
    using std::setw;
    using std::setfill;

    std::vector<char> buf;
    std::vector<uint8_t> b2buf;
    std::ifstream in(filename);

    if(!in) {
        cout << "Can't open file: " << filename << "\n";
        return;
    }

    const int chunk = 4;
    buf.resize(chunk);
    b2buf.resize(32);
    blake2sp_state b2state = {};
    blake2sp_init(&b2state, 32);

    while(true) {
        in.read(buf.data(), chunk);
        const int read = in.gcount();
        if(read == 0)
            break;

        (void) blake2sp_update(&b2state, buf.data(), read);

        if(read != chunk)
            break;
    }

    blake2sp_final(&b2state, b2buf.data(), 32);
    for(int i = 0; i < 32; i++) {
        cout << hex << setw(2) << setfill('0') << (int) b2buf[i] << setw(0);
    }

    cout << "  " << filename << "\n";
}

int main(int argc, char **argv) {
    using namespace std;

    if(argc < 2) {
        cout << "Enter filenames as arguments.\n";
        return 1;
    }

    for(int i = 1; i < argc; i++) {
        process(argv[i]);
    }

    return 0;
}
