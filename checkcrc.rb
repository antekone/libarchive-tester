require 'zlib'

to_read = nil
crc32 = 0
size = 0

fr = File.open("out.bin", "rb")

IO.readlines(ARGV[0]).each do |line|
    # update_crc: to_read=0x00000500
    if line =~ /^update_crc: to_read=0x([0-9A-Za-f]*)/
        to_read = $1.to_i 16
        state = nil
        next
    end

    # _crc32 state: 0x5c2302e4
    if line =~ /^_crc32 state: 0x([0-9A-Za-f]*)/
        state = $1.to_i 16
    end

    if state != nil
        puts "Read 0x%08x, state 0x%08x" % [to_read, state]

        data = fr.read to_read
        crc32 = Zlib::crc32 data, crc32
        size += data.size

        if crc32 != state
            puts "Different CRC state, mine is: 0x%08x" % crc32
            exit 1
        else
            puts "Proper CRC state: 0x%08x" % crc32
        end
    end
end

puts "Processed #{size} bytes."
