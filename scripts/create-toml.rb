require 'digest'

def cmd(line)
    buf = `#{line}`
    if $?.exitstatus != 0 then
        puts("Command failed: #{line}")
        puts("Full output:")
        puts(buf)
        exit(1)
    end
    return buf
end

def calc(fn)
  hash = Digest::SHA2.new(512)
  fp = File.open(fn)
  loop do
    data = fp.read(4096)
    break if data == nil
    hash.update(data)
    break if data.size() != 4096
  end
  hash.hexdigest
end

def main(filename)
    cmd("rm -rf tempdir")
    buf = cmd("aunpack #{filename} -X tempdir").split("\n")

    basename = File.basename(filename, File.extname(filename))

    puts "[[testcase.entry]]"
    puts "name = \"#{basename}\""
    puts "input_files = [\"#{filename}\"]"
    puts ""

    buf.each() do |line|
      if line =~ /^Extracting[ ]*(.*?)    /
        hash = calc($1)
        size = File.open($1).size
        name = $1.gsub("tempdir/", "")
        puts "[[testcase.entry.expect]]"
        puts "type = \"valid\""
        puts "size = #{size}"
        puts "name = \"#{name}\""
        puts "sha512 = \"#{hash}\""
        puts ""
      end
    end

    cmd("rm -rf tempdir")
end

aunpack = `which aunpack`.strip
unrar = `which unrar`.strip

if aunpack.size == 0 then
    puts("The 'aunpack' command is missing")
    exit(1)
end

if unrar.size == 0 then
    puts("The 'unrar' command is missing")
    exit(1)
end

if $*.size == 0 then
    puts("Give me a filename!")
    exit(1)
end

fn = $*[0]

abspath = cmd("realpath #{fn}").strip
if !abspath.end_with?(".rar") then
    puts("Filename needs to end with '.rar'")
end

main(abspath)