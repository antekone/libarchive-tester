#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <memory.h>
#include <stdlib.h>
#include <stdint.h>
#include <libarchive/archive.h>
#include <libarchive/archive_entry.h>

// 1=ok, 0=error
int unpack(struct archive* a, struct archive_entry* e) {
    const char* filename = archive_entry_pathname_utf8(e);
    int chunks, i;
    const int buf_size = 4096;
    char* buf = malloc(buf_size);
    int ret;

    time_t atime = archive_entry_atime(e);
    time_t mtime = archive_entry_mtime(e);
    time_t _ctime = archive_entry_ctime(e);

    const uint64_t size = archive_entry_size(e);
    printf("[app] [unpack] Extracting '%s' (%lld bytes)\n", filename, size);

    if(atime)
        printf("[app] atime: %s\n", ctime(&atime));

    if(mtime)
        printf("[app] mtime: %s\n", ctime(&mtime));

    if(_ctime)
        printf("[app] ctime: %s\n", ctime(&_ctime));

    int mode = archive_entry_mode(e);
    printf("[app] mode=0x%08x\n", mode);
    int is_file = (archive_entry_mode(e) & AE_IFREG) > 0;
    int is_dir = (archive_entry_mode(e) & AE_IFDIR) > 0;
    printf("[app] file? %d, dir? %d\n", is_file, is_dir);
//    if(is_dir == 0 && is_file == 0) {
//        printf("[app] no dir, and no file?\n");
//        free(buf);
//        return 0;
//    }

    chunks = size / buf_size;
    FILE* fw = fopen("out.bin", "w");
    if(!fw) {
        printf("[app] can't create out.bin.\n");
        free(buf);
        return 0;
    }

    const uint64_t entry_size = archive_entry_size(e);
    printf("Extracting '%s' (%lld bytes)\n", filename, entry_size);
    chunks = size / buf_size;
    for(i = 0; i <= chunks; i++) {
        int chunk = i < chunks ? buf_size : (size % buf_size);
        ret = archive_read_data(a, buf, chunk);
        if(ret < 0) {
            printf("[app] chunk read error: %d, '%s'\n", ret, archive_error_string(a));
            free(buf);
            fclose(fw);
            return 0;
        }

        //printf("[app] chunk=%zu\n", chunk);
        fwrite(buf, chunk, 1, fw);
    }

    printf("[app] Extraction done.\n");

    fclose(fw);
    free(buf);
    return 1;
}

int main(int argc, char** argv) {
    if(argc < 2) {
        printf("[app] Specify filename in the argument.\n");
        return 1;
    }

    struct archive* a = archive_read_new();
    struct archive_entry* e;
    int ret, i;

    archive_read_support_format_all(a);

    if(strstr(argv[1], ".part") != 0) {
        char fbuf[4096];
        strcpy(fbuf, argv[1]);

        const char* filename = fbuf;
        char* p = strstr(filename, ".part");
        p[0] = 0;

        const char* file_table[9999];
        memset(file_table, 0, sizeof(file_table));

        int i = 1;

        while(1) {
            sprintf(p, ".part%04d.rar", i);
            if(access(filename, F_OK) != 0)
                break;

            file_table[i - 1] = strdup(filename);
            i++;
        }

        while(1) {
            sprintf(p, ".part%03d.rar", i);
            if(access(filename, F_OK) != 0)
                break;

            file_table[i - 1] = strdup(filename);
            i++;
        }

        while(1) {
            sprintf(p, ".part%02d.rar", i);
            if(access(filename, F_OK) != 0)
                break;

            file_table[i - 1] = strdup(filename);
            i++;
        }

        while(1) {
            sprintf(p, ".part%d.rar", i);
            if(access(filename, F_OK) != 0)
                break;

            file_table[i - 1] = strdup(filename);
            i++;
        }

        printf("[app] Loaded files:\n");

        i = 0;
        while(1) {
            if(file_table[i] == NULL)
                break;

            printf("[app] - '%s'\n", file_table[i]);
            i++;
        }

        ret = archive_read_open_filenames(a, file_table, 4096);
    } else {
        ret = archive_read_open_filename(a, argv[1], 128);
    }

    if(ret != ARCHIVE_OK) {
        printf("[app] Can't open file: %s\n", argv[1]);
        archive_free(a);
        return 1;
    }

    printf("[app] archive opened\n");

    while(1) {
        printf("[app] Next header\n");
        ret = archive_read_next_header(a, &e);
        if(ret == ARCHIVE_OK) {
            const char* filename = archive_entry_pathname_utf8(e);
            printf("[app] File: '%s'\n", filename);

            if(argc == 2) {
                if(!unpack(a, e)) {
                    printf("[app #1] unpack(): error: %s\n", archive_error_string(a));
                    archive_free(a);
                    return 1;
                }
            } else {
                for(i = 2; i < argc; i++) {
                    if(strcmp(argv[i], filename) == 0) {
                        if(!unpack(a, e)) {
                            printf("[app #2] unpack(): error\n");
                            archive_free(a);
                            return 1;
                        }
                    }
                }
            }
        } else if(ret == ARCHIVE_EOF) {
            printf("[app] eof\n");
            break;
        } else {
            printf("[app] archive_read_next_header(): error %d\n", ret);
            return 1;
        }
    }

    archive_read_free(a);
    return 0;
}
