#include <archive.h>
#include <vector>
#include <string>

#include "test_case.h"

enum class UnpackMethod {
    Fixed, Variable
};

static std::string to_string(UnpackMethod m) {
    switch (m) {
        case UnpackMethod::Fixed: return "Fixed";
        case UnpackMethod::Variable: return "Variable";
        default: return "?";
    }
}

class Context {
public:
    Context() {
        a = archive_read_new();
        archive_read_support_format_all(a);
    }

    Context(Context& other) = delete;
    Context* operator= (const Context*) = delete;

    ~Context() {
        if (a) {
            archive_read_close(a);
            archive_free(a);
        }
    }

    void copyToInput(std::vector<std::string> vec) {
        input.swap(vec);
    }

    [[nodiscard]] archive* getArchive() const { return a; }

    [[nodiscard]] size_t numberOfInputs() const { return input.size(); }
    const std::string& getInput(size_t i) { return input[i]; }

    [[nodiscard]] size_t getBlockSize() const { return blockSize; }
    void setBlockSize(size_t b) { blockSize = b; }

    [[nodiscard]] UnpackMethod getUnpackMethod() const { return method; }
    void setUnpackMethod(UnpackMethod um) { method = um; }

    void setUnitTestMode(bool f) { unitTesting = f; }
    [[nodiscard]] bool getUnitTestMode() const { return unitTesting; }

    void setNoSizeCheck(bool f) { noSizeCheck = f; }
    [[nodiscard]] bool getNoSizeCheck() const { return noSizeCheck; }

    void setNoErrorCheck(bool f) { noErrorCheck = f; }
    [[nodiscard]] bool getNoErrorCheck() const { return noErrorCheck; }

    void reinitLibarchive() {
        if (a) {
            archive_read_close(a);
            archive_free(a);
        }

        a = archive_read_new();
        archive_read_support_format_all(a);
    }

    std::vector<std::shared_ptr<TestCaseExpectation>>::const_iterator curExpectation;

private:
    std::vector<std::string> input;
    archive* a = nullptr;
    size_t blockSize = 4096;
    bool unitTesting = false;
    UnpackMethod method;
    bool noSizeCheck = false;
    bool noErrorCheck = false;
};

using ContextRef = std::shared_ptr<Context>;
