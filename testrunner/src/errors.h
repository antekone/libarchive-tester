#include <algorithm>
#include <fmt/core.h>
#include <cstdlib>
#include <archive.h>

template <typename Base>
class FormatAware {
public:
    template <typename... Args>
    inline static Base formatted(Args&&... args) {
        return Base(fmt::format(std::forward<Args>(args)...));
    }
};

static std::string archiveErrorString(archive* a) {
    if (!a) return "(a == nullptr)";
    const char* errorMsg = archive_error_string(a);
    if (!errorMsg) return "(no error)";
    return errorMsg;
}

class RunnerError : public std::exception {
public:
    std::string msg;

    template <typename... Args>
    explicit RunnerError(std::string m) :
        msg(std::move(m)), std::exception()
    {
    }

    [[nodiscard]] const char* what() const noexcept override {
        return msg.c_str();
    }
};

class WrongArgsError : public RunnerError, public FormatAware<WrongArgsError> {
public:
    explicit WrongArgsError(std::string msg) : RunnerError(std::move(msg)) {}

    static WrongArgsError silent() {
        auto err = WrongArgsError("");
        err.silentMode = true;
        return err;
    }

    [[nodiscard]] bool isSilent() const { return silentMode; }

private:
    bool silentMode = false;
};

class ConfigError : public RunnerError, public FormatAware<ConfigError> {
public:
    explicit ConfigError(std::string msg) : RunnerError(std::move(msg)) {}
};

class ArchiveError : public RunnerError, public FormatAware<ArchiveError> {
public:
    explicit ArchiveError(std::string msg) : RunnerError(std::move(msg)) { }
    explicit ArchiveError(archive* a) : RunnerError(archiveErrorString(a)) { }

    ArchiveError(const std::string& msg, archive* a) :
        RunnerError(fmt::format("{}: {}", msg, archiveErrorString(a))) { }
};

class ExpectationFailed : public RunnerError, public FormatAware<ExpectationFailed> {
public:
    explicit ExpectationFailed(std::string msg) : RunnerError(std::move(msg)) { }
    explicit ExpectationFailed(archive* a) : RunnerError(archiveErrorString(a)) { }

    ExpectationFailed(const std::string& msg, archive* a) :
        RunnerError(fmt::format("{}: {}", msg, archiveErrorString(a))) { }
};