#include <fmt/core.h>
#include <fmt/color.h>

template <typename... Args>
void log(std::string fmt, Args&&... args) {
    fmt = "[runner] " + fmt + "\n";
    fmt::print(fmt, std::forward<Args>(args)...);
}

template <typename... Args>
void log(fmt::text_style col, std::string fmt, Args&&... args) {
    fmt = "[runner] " + fmt + "\n";
    fmt::print(col, fmt, std::forward<Args>(args)...);
}
