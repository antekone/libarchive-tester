#include <cstdio>
#include <memory>

#define FMT_HEADER_ONLY

#include <archive.h>
#include <archive_entry.h>
#include <cxxopts.hpp>
#include <fmt/core.h>
#include <fmt/format.h>
#include <gtest/gtest.h>
#include <rhash.h>

#include "context.h"
#include "utils.h"
#include "errors.h"
#include "test_case.h"
#include "log.h"

static const char* ARG_INPUT = "input";
static const char* ARG_HELP = "help";
static const char* ARG_DRY_RUN = "dry-run";
static const char* ARG_BLOCK_SIZE = "block-size";
static const char* ARG_UNPACK_METHOD = "unpack-method";
static const char* ARG_UNIT_TESTS = "unit-tests";
static const char* ARG_NO_SIZE_CHECK = "no-size-check";
static const char* ARG_NO_ERROR_CHECK = "no-error-check";
static const char* ARG_ASAN_TEST = "asan-test";

void parseOptions(ContextRef& ctx, int argc, char** argv) {
    try {
        cxxopts::Options options("TestRunner", "Test runner for libarchive");
        options.add_options()
            (fmt::format("h,{}", ARG_HELP), "Display this help screen")
            (fmt::format("{}", ARG_ASAN_TEST), "Perform an ASAN test (make a memory leak and crash)")
            (fmt::format("i,{}", ARG_INPUT), "Unpack file specified by 'arg'", cxxopts::value<std::vector<std::string>>())
            (fmt::format("m,{}", ARG_UNPACK_METHOD), "Unpack method: 'fixed', 'variable' (default)", cxxopts::value<std::string>()->default_value("variable"))
            (fmt::format("b,{}", ARG_BLOCK_SIZE), "Set libarchive block size", cxxopts::value<size_t>()->default_value(fmt::format("{}", ctx->getBlockSize())))
            (fmt::format("{}", ARG_NO_SIZE_CHECK), "Skip size check in standalone mode")
            (fmt::format("{}", ARG_NO_ERROR_CHECK), "Don't stop on unpacking errors")
            (fmt::format("{}", ARG_UNIT_TESTS), "Only run unit tests")
            (fmt::format("n,{}", ARG_DRY_RUN), "Unpack, but don't store unpacked files");
        options.parse_positional(ARG_INPUT);
        options.positional_help("input files...");

        auto result = options.parse(argc, argv);
        if (result.count(ARG_HELP) > 0) {
            fmt::print("{}", options.help());
            throw WrongArgsError::silent();
        }

        if (result.count(ARG_INPUT) > 0) {
            const auto& vec = result[ARG_INPUT].as<std::vector<std::string>>();
            ctx->copyToInput(vec);
        }

        if (result.count(ARG_ASAN_TEST) > 0) {
            log("Performing a memory leak and crashing...");
            ::fflush(stdout);
            int* leak = new int[10];
            *((volatile uint8_t*) nullptr) = 123;
            exit(1);
        }

        if (result.count(ARG_BLOCK_SIZE) > 0) {
            auto blockSize = result[ARG_BLOCK_SIZE].as<size_t>();
            const size_t maxBlockSize = 1 * 1024 * 1024;
            if (blockSize > maxBlockSize)
                throw WrongArgsError::formatted("Block size too big (max is {} bytes)", maxBlockSize);

            ctx->setBlockSize(blockSize);
        }

        const auto& unpackMethod = result[ARG_UNPACK_METHOD].as<std::string>();
        if (unpackMethod == "fixed") {
            ctx->setUnpackMethod(UnpackMethod::Fixed);
        } else if (unpackMethod == "variable") {
            ctx->setUnpackMethod(UnpackMethod::Variable);
        } else {
            throw WrongArgsError::formatted("Unknown unpack method specified");
        }

        bool unitTestMode = result.count(ARG_UNIT_TESTS) > 0;
        ctx->setUnitTestMode(unitTestMode);
        ctx->setNoSizeCheck(result.count(ARG_NO_SIZE_CHECK) > 0);
        ctx->setNoErrorCheck(result.count(ARG_NO_ERROR_CHECK) > 0);
    } catch (cxxopts::OptionException& e) {
        throw WrongArgsError::formatted("Argument error: {}", e.what());
    }
}

std::vector<const char*> createFilenamesArray(ContextRef& ctx) {
    const auto len = ctx->numberOfInputs();
    std::vector<const char*> arr;

    for (int i = 0; i < len; i++) {
        const auto& f = ctx->getInput(i);
        if (endsWith(f, ".toml"))
            continue;

        arr.push_back(f.c_str());
    }

    arr.push_back(nullptr);
    return arr;
}

void maybeThrow(const ContextRef& ctx, const RunnerError& error) {
    if (ctx->getNoErrorCheck()) {
        log("ERROR (ignored): {}", error.what());
    } else {
        throw error;
    }
}

void unpackFixed(ContextRef& ctx, archive_entry* e) {
    const int64_t wholeSize = archive_entry_size(e);
    if (wholeSize < 0) {
        maybeThrow(ctx, ArchiveError("Size is less than 0", ctx->getArchive()));
    }

    log("- Size of this entry: {} bytes", wholeSize);
    if (wholeSize == 0) {
        log("- Size is zero, don't need to unpack this.");
        return;
    }

    const int bufSize = 16 * 1024;
    auto* buf = new uint8_t [bufSize];
    auto cleanup = make_scope_exit([buf] () {
        delete[] buf;
    });

    int64_t chunks = wholeSize / bufSize;
    uint64_t unpackedSize = 0;
    int rest = static_cast<int>(wholeSize % bufSize);

    log("- Reading file, using {} chunks of {} bytes, last chunk has {} bytes",
        chunks, bufSize, rest);

    for (int i = 0; i <= chunks; i++) {
        size_t toRead = i < chunks ? bufSize : rest;
        ssize_t ret = archive_read_data(ctx->getArchive(), buf, toRead);
        if (ret > 0) {
            unpackedSize += ret;
        }

        if (ret == 0) {
            maybeThrow(ctx, ArchiveError("archive_read_data() has returned 0 bytes", ctx->getArchive()));
        } else if (ret < 0) {
            maybeThrow(ctx, ArchiveError::formatted("archive_read_data() has returned {}: {}", ret, archive_error_string(ctx->getArchive())));
        } else if (ret != toRead) {
            if (ctx->getNoSizeCheck()) {
                int64_t declaredSize = archive_entry_size(e);
                if (declaredSize != unpackedSize) {
                    log("- Finished unpacking, full size is {} bytes (DOES NOT match the declared value)", unpackedSize);
                } else {
                    log("- Finished unpacking, full size is {} bytes (matches the declared value)", unpackedSize);
                }
            } else {
                maybeThrow(ctx, ArchiveError::formatted("archive_read_data({}) returned {}", toRead, ret));
            }
        }
    }
}

void unpackVariable(ContextRef& ctx, archive_entry* e) {
    const int bufSize = 16 * 1024;
    auto* buf = new uint8_t [bufSize];
    auto cleanup = make_scope_exit([buf] () {
        delete[] buf;
    });

    const int64_t wholeSize = archive_entry_size(e);
    if (wholeSize == 0) {
        log("- Bugfix: not unpacking file with size == 0");
        return;
    }
    
    log("- Reading file, using unknown number of chunks");

    int64_t unpackedSize = 0;
    do {
        ssize_t ret = archive_read_data(ctx->getArchive(), buf, bufSize);
        if (ret < 0) {
            maybeThrow(ctx, ArchiveError::formatted("archive_read_data() has returned {}: {}", ret, archive_error_string(ctx->getArchive())));
        } else if (ret == 0) {
            break;
        } else {
            unpackedSize += ret;
        }
    } while(true);

    if (!ctx->getNoSizeCheck()) {
        int64_t declaredSize = archive_entry_size(e);
        if (declaredSize != unpackedSize) {
            maybeThrow(ctx, ArchiveError::formatted("Declared size is {}, but unpacked size is {}",
                                          declaredSize, unpackedSize));
        }

        log("- Finished unpacking, full size is {} bytes (matches the declared value)", unpackedSize);
    } else {
        int64_t declaredSize = archive_entry_size(e);
        if (declaredSize != unpackedSize) {
            log("- Finished unpacking, full size is {} bytes (DOES NOT match the declared value)", unpackedSize);
        } else {
            log("- Finished unpacking, full size is {} bytes (matches the declared value)", unpackedSize);
        }
    }

}

void unpack(ContextRef& ctx, archive_entry* e) {
    switch (ctx->getUnpackMethod()) {
        case UnpackMethod::Fixed:
            unpackFixed(ctx, e);
            break;
        case UnpackMethod::Variable:
            unpackVariable(ctx, e);
            break;
    }
}

bool iterate(ContextRef& ctx) {
    log("- Next header");

    archive_entry* e = nullptr;
    int ret = archive_read_next_header(ctx->getArchive(), &e);
    if (ret == ARCHIVE_OK) {
        const char* filename = archive_entry_pathname_utf8(e);
        log("- Got filename: '{}'", filename);
        unpack(ctx, e);
    } else if (ret == ARCHIVE_EOF) {
        log("- EOF encountered");
        return false;
    } else {
        log("- Custom error encountered: {}", ret);
        throw ArchiveError(ctx->getArchive());
    }

    return true;
}

int runUnitTests(ContextRef& ctx, int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

void runRawFiles(ContextRef& ctx, const std::vector<const char*>& filenames) {
    if (filenames.size() <= 1) {
        throw WrongArgsError::formatted("No valid input files provided, can't continue.");
    }

    log("- Trying to open all {} files (using block size {})...", filenames.size() - 1, ctx->getBlockSize());

    int ret = archive_read_open_filenames(
        ctx->getArchive(), const_cast<const char**>(filenames.data()), ctx->getBlockSize());
    if (ret != ARCHIVE_OK) {
        throw ArchiveError("archive_read_open_filenames", ctx->getArchive());
    }

    log("- Archive(s) opened");

    int iteration = 0;
    while (iterate(ctx)) {
        log("");
        log("# Iteration {}", iteration);
        iteration++;
    }
}

void run(ContextRef& ctx) {
    if (ctx->numberOfInputs() == 0) {
        throw WrongArgsError::formatted("No input files provided, nothing to do. Use `-h` to get some help.");
    }

    const auto filenames = createFilenamesArray(ctx);
    if (filenames.size() > 1) {
        runRawFiles(ctx, filenames);
    }

    const auto tomlFiles = TestCase::createTomlQueue(ctx);
    if (!tomlFiles.empty()) {
        ctx->setUnpackMethod(UnpackMethod::Fixed);
        TestCase::runTomlQueue(ctx, tomlFiles);
        ctx->reinitLibarchive();
        ctx->setUnpackMethod(UnpackMethod::Variable);
        TestCase::runTomlQueue(ctx, tomlFiles);
    }

    log("- Done.");
}

int main(int argc, char** argv) {
    rhash_library_init();

    try {
        auto ctx = std::make_shared<Context>();
        parseOptions(ctx, argc, argv);

        if (ctx->getUnitTestMode()) {
            return runUnitTests(ctx, argc, argv);
        } else {
            run(ctx);
            return 0;
        }
    } catch (WrongArgsError& e) {
        if (!e.isSilent()) {
            log("Argument error: {0}", e.what());
            log("Use `-h` option to get some help about arguments.");
        }
    } catch (RunnerError& e) {
        log("Runtime error: {0}", e.what());
        log("Use `-h` option to get some help about arguments.");
    }

    return 1;
}