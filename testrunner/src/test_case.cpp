#include <cstdio>
#include <memory>
#include <optional>
#include <algorithm>
#include <fmt/color.h>

#include <toml11/toml.hpp>
#include <archive.h>
#include <archive_entry.h>
#include <rhash.h>

#include "test_case.h"
#include "errors.h"
#include "log.h"
#include "context.h"
#include "utils.h"

std::shared_ptr<TestCaseExpectation> createValidExpectation(const toml::table& t) {
    const auto name = t.at("name").as_string();
    const auto size = t.at("size").as_integer();
    const auto sha512 = t.at("sha512").as_string();

    auto exp = std::make_shared<ValidTestCaseExpectation>();
    exp->fileName = name;
    exp->fileSize = size;
    exp->sha512 = sha512;
    return exp;
}

std::shared_ptr<TestCaseExpectation> createInvalidExpectation(const toml::table& t) {
    auto exp = std::make_shared<InvalidTestCaseExpectation>();

    if (t.count("repetitions") > 0) {
        const auto repetitions = t.at("repetitions").as_integer();
        exp->repetitions = static_cast<int>(repetitions);
    } else {
        exp->repetitions = 1;
    }

    return exp;
}

std::shared_ptr<TestCaseExpectation> createSkipExpectation(const toml::table& t) {
    auto exp = std::make_shared<IgnoreTestCaseExpectation>();

    if (t.count("repetitions") > 0) {
        const auto repetitions = t.at("repetitions").as_integer();
        exp->repetitions = static_cast<int>(repetitions);
    } else {
        exp->repetitions = 1;
    }

    return exp;
}

std::shared_ptr<TestCaseExpectation> createNoExpectation(const toml::table& t) {
    auto exp = std::make_shared<NoTestCaseExpectation>();

    if (t.count("repetitions") > 0) {
        const auto repetitions = t.at("repetitions").as_integer();
        exp->repetitions = static_cast<int>(repetitions);
    } else {
        exp->repetitions = 1;
    }

    return exp;
}

static std::vector<std::shared_ptr<TestCaseExpectation>> createExpectations(
    const toml::array& entries
) {
    std::vector<std::shared_ptr<TestCaseExpectation>> arr;

    for (const auto& e : entries) {
        const auto entryType = toml::find(e, "type").as_string();
        const auto& table = e.as_table();

        if (entryType == "valid") {
            arr.push_back(createValidExpectation(table));
        } else if (entryType == "invalid") {
            arr.push_back(createInvalidExpectation(table));
        } else if (entryType == "skip") {
            arr.push_back(createSkipExpectation(table));
        } else if (entryType == "off") {
            arr.push_back(createNoExpectation(table));
        }
    }

    return arr;
}

std::vector<TestCase> TestCase::createTomlQueue(ContextRef& ctx) {
    const auto len = ctx->numberOfInputs();
    std::vector<TestCase> arr;

    for (int i = 0; i < len; i++) {
        const auto& f = ctx->getInput(i);
        if (!endsWith(f, ".toml"))
            continue;

        const auto tcs = TestCase::createFromToml(f);
        for (const auto& tc : tcs) {
            arr.push_back(tc);
        }
    }

    return arr;
}

std::vector<TestCase> TestCase::createFromToml(const std::string& filename) {
    std::vector<TestCase> arr;

    try {
        const auto data = toml::parse(filename);
        const auto testcase = toml::find(data, "testcase");
        if (!testcase.is_table()) {
            throw ConfigError::formatted("Missing 'testcase' root");
        }

        const auto entries = toml::find(testcase, "entry");
        if (entries.size() == 0) {
            throw ConfigError::formatted("Missing 'testcase.entry' entries");
        }

        for (const auto& entry : entries.as_array()) {
            const auto& entryTable = entry.as_table();

            std::vector<std::string> fileNames;
            if (entryTable.count("input_files") == 0) {
                throw ConfigError::formatted("TestCase is missing 'input_files' array");
            }

            for (const auto& fileName : entryTable.at("input_files").as_array()) {
                fileNames.push_back(fileName.as_string());
            }

            const auto testcaseEntryName = entryTable.at("name");

            TestCase tc{};
            tc.inputFiles = std::move(fileNames);
            tc.name = testcaseEntryName.as_string();
            tc.expectations = createExpectations(toml::find(entry, "expect").as_array());
            arr.emplace_back(tc);
        }
    } catch (toml::syntax_error& e) {
        throw ConfigError::formatted("TOML syntax error: {}", e.what());
    }

    return arr;
}

std::pair<uint64_t, std::string> unpackFixedTestCase(ContextRef& ctx, archive_entry* e) {
    rhash hashCtx = rhash_init(RHASH_SHA512);
    if (!hashCtx) {
        throw ExpectationFailed("error when initializing sha512 rhash context");
    }

    auto cleanupRHash = make_scope_exit([hashCtx] () {
        rhash_free(hashCtx);
    });

    const int64_t wholeSize = archive_entry_size(e);
    if (wholeSize < 0) {
        throw ArchiveError("Size is less than 0", ctx->getArchive());
    }
    
    if (wholeSize > 0) {
        const int bufSize = 16 * 1024;
        auto* buf = new uint8_t[bufSize];
        auto cleanupBuf = make_scope_exit([buf]() {
            delete[] buf;
        });

        int64_t chunks = wholeSize / bufSize;
        int rest = static_cast<int>(wholeSize % bufSize);

        log(fg(fmt::color::green), "- Reading file, using {} chunks of {} bytes, last chunk has {} bytes",
            chunks, bufSize, rest);

        for (int i = 0; i <= chunks; i++) {
            size_t toRead = i < chunks ? bufSize : rest;
            ssize_t ret = archive_read_data(ctx->getArchive(), buf, toRead);
            if (ret == 0 && toRead != 0) {
                throw ExpectationFailed("archive_read_data() has returned 0 bytes", ctx->getArchive());
            } else if (ret < 0) {
                throw ExpectationFailed::formatted("archive_read_data() has returned {}: {}", ret,
                                                   archive_error_string(ctx->getArchive()));
            } else if (ret != toRead) {
                throw ExpectationFailed::formatted("archive_read_data({}) returned {}", toRead, ret);
            }

            if (0 != rhash_update(hashCtx, buf, toRead)) {
                throw ExpectationFailed("error when updating rhash context");
            }
        }
    }

    if (0 != rhash_final(hashCtx, nullptr)) {
        throw ExpectationFailed("error when finalizing rhash context");
    }

    std::string hashBuf;
    hashBuf.resize(256);
    rhash_print(hashBuf.data(), hashCtx, 0, RHPR_HEX);
    hashBuf.resize(128);
    std::transform(hashBuf.begin(), hashBuf.end(), hashBuf.begin(), [] (char ch) { return std::tolower(ch); });

    return std::make_pair(wholeSize, hashBuf);
}

std::pair<uint64_t, std::string> unpackVariableTestCase(ContextRef& ctx, archive_entry* e) {
    rhash hashCtx = rhash_init(RHASH_SHA512);
    if (!hashCtx) {
        throw ExpectationFailed("error when initializing sha512 rhash context");
    }

    auto cleanupRHash = make_scope_exit([hashCtx] () {
        rhash_free(hashCtx);
    });

    const int64_t wholeSize = archive_entry_size(e);
    if (wholeSize < 0) {
        throw ArchiveError("Size is less than 0", ctx->getArchive());
    }

    int64_t unpackedSize = 0;
    if (wholeSize > 0) {
        const int bufSize = 16 * 1024;
        auto* buf = new uint8_t[bufSize];
        auto cleanup = make_scope_exit([buf]() {
            delete[] buf;
        });

        log(fg(fmt::color::green), "- Reading file, using unknown number of chunks");

        do {
            ssize_t ret = archive_read_data(ctx->getArchive(), buf, bufSize);
            if (ret == 0) {
                break;
            } else if (ret < 0) {
                throw ExpectationFailed::formatted("archive_read_data() has returned {}: {}", ret,
                                                   archive_error_string(ctx->getArchive()));
            } else {
                unpackedSize += ret;

                if (0 != rhash_update(hashCtx, buf, ret)) {
                    throw ExpectationFailed("error when updating rhash context");
                }
            }
        } while (true);
    }

    if (0 != rhash_final(hashCtx, nullptr)) {
        throw ExpectationFailed("error when finalizing rhash context");
    }
    
    std::string hashBuf;
    hashBuf.resize(256);
    rhash_print(hashBuf.data(), hashCtx, 0, RHPR_HEX);
    hashBuf.resize(128);
    std::transform(hashBuf.begin(), hashBuf.end(), hashBuf.begin(), [] (char ch) { return std::tolower(ch); });

    return std::make_pair(unpackedSize, hashBuf);
}

std::pair<uint64_t, std::string> unpackTestCase(ContextRef& ctx, archive_entry* e) {
    switch (ctx->getUnpackMethod()) {
        case UnpackMethod::Fixed:
            return unpackFixedTestCase(ctx, e);
        case UnpackMethod::Variable:
            return unpackVariableTestCase(ctx, e);
        default:
            throw ExpectationFailed::formatted("Unsupported UnpackMethod in unpackTestCase");
    }
}

void processInvalidExpectation(
    ContextRef& ctx,
    const TestCase& tc,
    const std::shared_ptr<InvalidTestCaseExpectation>& exp)
{
    log(fg(fmt::color::green), "- Expecting an invalid seek to the a new header");

    archive_entry* e = nullptr;
    int ret = archive_read_next_header(ctx->getArchive(), &e);
    if (ret == ARCHIVE_OK) {
        throw ExpectationFailed(
            "Expected archive_read_next_header to fail, but it has succeeded");
    }
}

void processSkipExpectation(
    ContextRef& ctx,
    const TestCase& tc,
    const std::shared_ptr<IgnoreTestCaseExpectation>& exp)
{
    log(fg(fmt::color::green), "- Expecting an successful seek to the a new header, but skipping"
        " the unpacking process.");

    archive_entry* e = nullptr;
    int ret = archive_read_next_header(ctx->getArchive(), &e);
    if (ret != ARCHIVE_OK) {
        throw ExpectationFailed(
            "Expected archive_read_next_header to succeed, but it failed",
            ctx->getArchive());
    }
}

void processNoExpectation(
    ContextRef& ctx,
    const TestCase& tc,
    const std::shared_ptr<NoTestCaseExpectation>& exp)
{
    log(fg(fmt::color::green), "- Skipping read to next header, ignoring any possible errors.");

    archive_entry* e = nullptr;
    std::ignore = archive_read_next_header(ctx->getArchive(), &e);
}

void processValidExpectation(
    ContextRef& ctx,
    const TestCase& tc,
    const std::shared_ptr<ValidTestCaseExpectation>& exp)
{
    log(fg(fmt::color::green), "- Expecting a successful seek to a new header");

    archive_entry* e = nullptr;
    int ret = archive_read_next_header(ctx->getArchive(), &e);
    if (ret != ARCHIVE_OK) {
        throw ExpectationFailed(
            "Expected archive_read_next_header to succeed, but it failed",
            ctx->getArchive());
    }

    log(fg(fmt::color::green), "- Expecting a successful filename");

    const char* filename = archive_entry_pathname_utf8(e);
    if (!filename) {
        throw ExpectationFailed(
            "Expected a filename, but got a null pointer", ctx->getArchive());
    }

    if (0 != memcmp(
        filename,
        exp->fileName.c_str(),
        std::min(strlen(filename), exp->fileName.size())))
    {
        log(fg(fmt::color::red), "- Expected: {}", exp->fileName);
        log(fg(fmt::color::red), "- Got:      {}", filename);
        throw ExpectationFailed::formatted(
            "Expecting a valid path, but got an invalid path (see logs above)", filename);
    }

    log(fg(fmt::color::green), "- Expecting a successful decompression");

    try {
        auto[unpackedSize, hash] = unpackTestCase(ctx, e);
        if (unpackedSize != exp->fileSize) {
            throw ExpectationFailed::formatted("Expected decompression of {} bytes, but unpacked {}", unpackedSize,
                                               exp->fileSize);
        }

        if (0 != memcmp(hash.c_str(), exp->sha512.c_str(), std::min(hash.size(), exp->sha512.size()))) {
            log(fg(fmt::color::red), "- Expected: {}", exp->sha512);
            log(fg(fmt::color::red), "- Got:      {}", hash);
            throw ExpectationFailed::formatted("Expected hashes to be the same, but they aren't (see logs above)");
        }
    } catch (ExpectationFailed& e) {
        throw ExpectationFailed::formatted("{} (during unpacking '{}')", e.what(), filename);
    }
}

bool iterateTestCase(ContextRef& ctx, const TestCase& tc) {
    log(fg(fmt::color::green), "- {}: Next header", tc.name);

    ctx->curExpectation = tc.expectations.begin();
    int repetition = 0;
    int expectations = 0;

    while (ctx->curExpectation != tc.expectations.end()) {
        const auto& exp = *ctx->curExpectation;

        log(fg(fmt::color::green), "- Running expectation: {}", exp->getTypeAsString());

        if (exp->typeValid()) {
            processValidExpectation(ctx, tc, std::static_pointer_cast<ValidTestCaseExpectation>(exp));
            ctx->curExpectation++;
            expectations++;
            repetition = 0;
        } else if (exp->typeInvalid()) {
            processInvalidExpectation(ctx, tc, std::static_pointer_cast<InvalidTestCaseExpectation>(exp));
            if (repetition > 0) {
                repetition--;
            } else {
                ctx->curExpectation++;
            }
        } else if (exp->typeSkip()) {
            processSkipExpectation(ctx, tc, std::static_pointer_cast<IgnoreTestCaseExpectation>(exp));
            if (repetition > 0) {
                repetition--;
            } else {
                ctx->curExpectation++;
            }
        } else if (exp->typeOff()) {
            processNoExpectation(ctx, tc, std::static_pointer_cast<NoTestCaseExpectation>(exp));
            if (repetition > 0) {
                repetition--;
            } else {
                ctx->curExpectation++;
            }
        } else {
            throw ConfigError::formatted("Invalid expectation object, internal error!");
        }
    }

    log(fg(fmt::color::green), "- Processed {} expectations, no failures.", expectations);
    return false;
}

void runTestCase(ContextRef& ctx, const TestCase& tc) {
    const auto len = tc.inputFiles.size();
    std::vector<const char*> arr;

    for (int i = 0; i < len; i++) {
        const auto& f = tc.inputFiles[i];
        arr.push_back(f.c_str());
        log(fg(fmt::color::green), "- {}: registering input file: '{}'", tc.name, f);
    }

    arr.push_back(nullptr);

    int ret = archive_read_open_filenames(
        ctx->getArchive(), const_cast<const char**>(arr.data()), ctx->getBlockSize());
    if (ret != ARCHIVE_OK) {
        throw ArchiveError("archive_read_open_filenames", ctx->getArchive());
    }

    log(fg(fmt::color::green), "- {}: archive(s) opened", tc.name);

    iterateTestCase(ctx, tc);
}

void TestCase::runTomlQueue(ContextRef& ctx, const std::vector<TestCase>& testcases) {
    for (const auto& tc : testcases) {
        log("");
        log(fg(fmt::color::green_yellow), "*** Running testcase: {}", tc.name);
        log(fg(fmt::color::green), "- {}: Unpack method: {}", tc.name, to_string(ctx->getUnpackMethod()));

        runTestCase(ctx, tc);
        ctx->reinitLibarchive();
    }
}
