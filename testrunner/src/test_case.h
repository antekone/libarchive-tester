#pragma once

class TestCaseExpectation {
public:
    explicit TestCaseExpectation(int typeId) : typeId(typeId) { }

    [[nodiscard]] bool typeValid() const { return typeId == 1; }
    [[nodiscard]] bool typeInvalid() const { return typeId == 2; }
    [[nodiscard]] bool typeSkip() const { return typeId == 3; }
    [[nodiscard]] bool typeOff() const { return typeId == 4; }

    [[nodiscard]] std::string getTypeAsString() const {
        switch (typeId) {
            case 1: return "valid";
            case 2: return "invalid";
            case 3: return "skip";
            case 4: return "off";
            default: return "?";
        }
    }

private:
    int typeId = 0;
};

class Context;
using ContextRef = std::shared_ptr<Context>;

class ValidTestCaseExpectation : public TestCaseExpectation {
public:
    ValidTestCaseExpectation() : TestCaseExpectation(1) { }

    std::string fileName;
    uint64_t fileSize = 0;
    std::string sha512;
};

class InvalidTestCaseExpectation : public TestCaseExpectation {
public:
    InvalidTestCaseExpectation() : TestCaseExpectation(2) { }

    int repetitions = 0;
};

class IgnoreTestCaseExpectation : public TestCaseExpectation {
public:
    IgnoreTestCaseExpectation() : TestCaseExpectation(3) { }

    int repetitions = 0;
};

class NoTestCaseExpectation : public TestCaseExpectation {
public:
    NoTestCaseExpectation() : TestCaseExpectation(4) { }

    int repetitions = 0;
};

class TestCase {
public:
    std::string name;
    std::vector<std::string> inputFiles;
    std::vector<std::shared_ptr<TestCaseExpectation>> expectations;

    static std::vector<TestCase> createTomlQueue(ContextRef& ctx);
    static void runTomlQueue(ContextRef& ctx, const std::vector<TestCase>& testcases);

private:
    static std::vector<TestCase> createFromToml(const std::string& filename);
};
