#include <gtest/gtest.h>
#define FMT_HEADER_ONLY
#include <fmt/format.h>
#include "../utils.h"

TEST(EndsWith, extension) {
    ASSERT_TRUE(endsWith("filename.toml", ".toml"));
    ASSERT_TRUE(endsWith("filename.toml", "toml"));
    ASSERT_TRUE(endsWith("filename.toml", "oml"));
    ASSERT_TRUE(endsWith("filename.toml", "ml"));
    ASSERT_TRUE(endsWith("filename.toml", "l"));

    ASSERT_FALSE(endsWith("filename.toml", "ztoml"));
    ASSERT_FALSE(endsWith("filename.toml", "o"));
    ASSERT_FALSE(endsWith("filename.toml", "m"));
    ASSERT_FALSE(endsWith("filename.toml", ""));
    ASSERT_FALSE(endsWith("filename.toml", ".txt"));
    ASSERT_FALSE(endsWith("filename", ".toml"));
    ASSERT_FALSE(endsWith("", ".toml"));
    ASSERT_FALSE(endsWith("", ""));
}