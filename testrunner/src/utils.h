#include <algorithm>

template <typename T>
struct scope_exit {
    explicit scope_exit(T&& t) : t_{std::move(t)} {}
    ~scope_exit() { t_(); }
    T t_;
};

template <typename T>
scope_exit<T> make_scope_exit(T&& t) {
    return scope_exit<T>{std::forward<T>(t)};
}

static bool endsWith(const std::string& s, const std::string& with) {
    auto sSize = s.size();
    auto withSize = with.size();

    if (with.empty()) return false;
    if (s.empty()) return false;

    for (int i = 1; i <= std::min(sSize, withSize); i++) {
        char sCh = s[sSize - i];
        char withCh = with[withSize - i];
        if (sCh != withCh)
            return false;
    }

    return true;
}